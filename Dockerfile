### Stage 1 - Build

FROM node:alpine as build

WORKDIR /workdir
COPY package.json ./
RUN npm install
COPY . .
RUN npm run build core --
RUN npm run build charts --
RUN npm run build webclient --

### Stage 2a - Serve client

FROM nginx:alpine as client
EXPOSE 80

COPY config/nginx/** /etc/nginx/conf.d/

WORKDIR /usr/share/nginx/html
COPY --from=build /workdir/dist/apps/webclient .

### Stage 2b - Serve server

FROM node:alpine as server
EXPOSE 3000

WORKDIR /workdir
COPY package.json ./
RUN npm install

COPY ./database.json .
COPY ./users.json .
COPY ./server.js .

ENTRYPOINT ["node", "server.js"]
