# Webclient

This project is an Angular 14 project using TypeScript 4.7. It's a generic PWA webclient styled using @angular/material. It may be used as a starting point when developing webclients using the above mentioned technologies.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files. Or launch Chrome from VS Code (F5 - start debugging) with
the [Debugger for Chrome extension](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome)
installed.

## Back end server

The webclient requires a back end serving some data (JSON formatted). The easiest way to do this is by using
[JSON Server](https://github.com/typicode/json-server). It is included as a devDependency in `package.json` along with
[JSON Web Token](https://github.com/auth0/node-jsonwebtoken) and a "wrapper" script inspired by [JSONServer + JWT Auth](https://github.com/techiediaries/fake-api-jwt-json-server) used as a mock Oauth2 authorization server.

Launch the back end server:

```bash
node server.js
```

...or launch the front and back end server in one single command:

```bash
npm run serve
```

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use
`ng generate directive | pipe | service | class | guard | interface | enum | module`.

## Build

Run `ng build` or `npm run build` to build the project. AoT (Ahead of Time compilation) is enabled by default. The build
artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build, witch includes service
workers.

## Building and running as docker images

Building the docker front-end image:

`docker build --target=fe -t public/fe:14.0.0 .`

Building the docker back-end image:

`docker build --target=bee -t public/be:14.0.0 .`

Running the docker images:

`docker run -d -p 8888:80 --rm --name public-fe public/fe:14.0.0`
`docker run -d -p 3000:3000 --rm --name public-be public/be:14.0.0`

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the
[Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
