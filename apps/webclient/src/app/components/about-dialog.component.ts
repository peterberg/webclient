// Import Angular stuff

import { Component, Inject } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { environment } from '../../environments/environment';

@Component({
  templateUrl: './about-dialog.component.html',
  styleUrls: ['./about-dialog.component.scss'],
})
export class AboutDialogComponent {
  public readonly environment = environment;

  constructor(private dialog: MatDialogRef<AboutDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {}

  public ok() {
    this.dialog.close();
  }
}
