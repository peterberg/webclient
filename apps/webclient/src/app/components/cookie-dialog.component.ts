// Import Angular stuff

import { Component, Inject } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { LocalStorage } from '@public/core';

@Component({
  templateUrl: './cookie-dialog.component.html',
  styleUrls: ['./cookie-dialog.component.scss'],
})
export class CookieDialogComponent {
  constructor(
    private storage: LocalStorage,
    private dialog: MatDialogRef<CookieDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  public ok() {
    this.storage.set('cookiesAccepted', new Date().toISOString());
    this.dialog.close();
  }
}
