// Import Angular stuff

import { Component, OnInit } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';

import { LocalStorage } from '@public/core';

import { CookieDialogComponent } from './cookie-dialog.component';

import { environment } from '../../environments/environment';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public readonly environment = environment;

  constructor(private dialog: MatDialog, private storage: LocalStorage) {}

  public ngOnInit() {
    // If use of cookies has not been accepted, show a dialog "forcing" the user to accept in order to proceed.

    if (!this.storage.get('cookiesAccepted')) {
      this.dialog.open(CookieDialogComponent, { disableClose: true });
    }
  }
}
