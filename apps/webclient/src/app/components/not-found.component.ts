// Import Angular stuff

import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
})
export class NotFoundComponent implements AfterViewInit {
  public audio = new Audio();

  constructor() {
    this.audio.src='/assets/test.wav';
    this.audio.load();
  }

  public ngAfterViewInit() {
    this.audio.play();
  }
}
