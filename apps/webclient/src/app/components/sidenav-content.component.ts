// Import Angular stuff

import { Component, OnInit } from '@angular/core';

// Import services used by the SidenavContentComponent

import { UserService } from '@public/core';

@Component({
  templateUrl: './sidenav-content.component.html',
  styleUrls: ['./sidenav-content.component.scss']
})
export class SidenavContentComponent implements OnInit {

  public currentUser: any;

  constructor(private userService: UserService) {
  }

  public ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();

    this.userService.userChange.subscribe(user => {
      this.currentUser = user;
    });
  }
}
