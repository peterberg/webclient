// Import Angular stuff

import { Component } from '@angular/core';

import { MatIconModule } from '@angular/material/icon';

// Import components and services used by the TermsOfUseComponent

import { ThemeService } from '@public/core/ui/theme';

@Component({
  selector: 'theme-toggle',
  templateUrl: './theme-toggle.component.html',
  styleUrls: ['./theme-toggle.component.scss']
})
export class ThemeToggleComponent {
 
  protected darkMode = false;

  constructor(private themeService: ThemeService) {
    this.darkMode = this.themeService.theme === 'dark';

    themeService.themeChange.subscribe(value => this.darkMode= value==='dark')
  }
  
  protected toggleTheme() {
    this.themeService.theme = this.themeService.theme === 'default' ? 'dark' : 'default';  
  }
}
