// Import Angular stuff

import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { MatDialog } from '@angular/material/dialog';

// Import services and components used by the ToolbarContentComponent

import { AlertService, UserService } from '@public/core';

import { LoginDialogComponent } from '@public/core/authentication';
import { OAuth2Service } from '@public/core/authorization';

import { ThemeService } from '@public/core/ui/theme';

import { AboutDialogComponent }from './about-dialog.component';

@Component({
  templateUrl: './toolbar-content.component.html',
  styleUrls: ['./toolbar-content.component.scss']
})
export class ToolbarContentComponent implements OnInit {
  public currentUser: any;

  constructor(
    public titleService: Title,
    private dialog: MatDialog,
    private readonly oauth2Service: OAuth2Service,
    private readonly userService: UserService,
    private readonly alertService: AlertService,
    private readonly themeService: ThemeService) {
  }

  public ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();

    this.userService.userChange.subscribe(user => {
      this.currentUser = user;
    });
  }
  
  public login() {
    this.dialog.open(LoginDialogComponent, { disableClose: true });
  }

  public logout() {
    this.oauth2Service.logout().subscribe(this.onLogoutSuccess,this.onLogoutFailure);
  }

  public about() {
    this.dialog.open(AboutDialogComponent, { disableClose: true });
  }

  public onLogoutSuccess = () => {
    this.alertService.success('You have been logged out.', 3000);
  }

  public onLogoutFailure = () => {
    this.alertService.error('An error occured while logging out.')
  }
}
