// Import Angular stuff

import { NgModule } from '@angular/core';

import { CdkTableModule } from '@angular/cdk/table';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';

// Import core functionality

import { CoreModule } from '@public/core';
import { LayoutModule } from '@public/core/ui/layout';

import { ChartsModule } from '@public/charts';

import { ChartsDemoRouting } from './charts-demo.routing';

import { ChartsOverviewComponent } from './components/overview.component';

import { BarChartDemoComponent } from './components/bar-chart-demo.component';
import { LineChartDemoComponent } from './components/line-chart-demo.component';
import { PieChartDemoComponent } from './components/pie-chart-demo.component';
import { RadarChartDemoComponent } from './components/radar-chart-demo.component';
import { ScatterChartDemoComponent } from './components/scatter-chart-demo.component';

@NgModule({
  imports: [
    CdkTableModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,

    CoreModule,
    LayoutModule,

    ChartsModule,

    ChartsDemoRouting,
  ],
  declarations: [
    ChartsOverviewComponent,
    BarChartDemoComponent,
    LineChartDemoComponent,
    PieChartDemoComponent,
    RadarChartDemoComponent,
    ScatterChartDemoComponent,
  ],
})
export class ChartsDemoModule {}
