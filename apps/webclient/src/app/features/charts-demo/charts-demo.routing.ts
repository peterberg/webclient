// Import Angular stuff

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Import shared functionality, including Material Design and Flex Layout

import { ChartsOverviewComponent } from './components/overview.component';

import { BarChartDemoComponent } from './components/bar-chart-demo.component';
import { LineChartDemoComponent } from './components/line-chart-demo.component';
import { PieChartDemoComponent } from './components/pie-chart-demo.component';
import { RadarChartDemoComponent } from './components/radar-chart-demo.component';
import { ScatterChartDemoComponent } from './components/scatter-chart-demo.component';

const routes: Routes = [
  {
    path: '',
    component: ChartsOverviewComponent
  },
  {
    path: 'bar',
    component: BarChartDemoComponent
  },
  {
    path: 'line',
    component: LineChartDemoComponent
  },
  {
    path: 'pie',
    component: PieChartDemoComponent
  },
  {
    path: 'radar',
    component: RadarChartDemoComponent
  },
  {
    path: 'scatter',
    component: ScatterChartDemoComponent
  },
  {
    path: '**',
    component: ChartsOverviewComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class ChartsDemoRouting {
}

