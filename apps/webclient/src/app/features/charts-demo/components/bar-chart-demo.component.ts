import { Component, OnInit } from '@angular/core';

import { labels } from './labels';
import { datasets } from './data';

@Component({
  templateUrl: './bar-chart-demo.component.html'
})
export class BarChartDemoComponent implements OnInit {
  public data: any = {
    labels,
    datasets
  };

  public ngOnInit() {
    this.data.datasets[0].backgroundColor= 'rgba(255,127,127,0.5)'
    this.data.datasets[1].backgroundColor= 'rgba(127,255,127,0.5)'
  }
}
