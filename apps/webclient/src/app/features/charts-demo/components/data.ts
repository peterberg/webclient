export const datasets = [
  {
    label: 'Data Set 1',
    data: [65, 59, 80, 81, 56, 55, 40, 56, 67, 89, 92, 76]
  },
  {
    label: 'Data Set 2',
    data: [65, 56, 34, 81, 95, 23, 17, 16, 7, 28, 62, 86]
  }

]
