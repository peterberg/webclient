import { Component } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { Position } from '@public/core';

import { Device } from '@public/core/ui/device';
import { PositionService } from '@public/core';

@Component({
  templateUrl: './device-demo.component.html',
  styleUrls: ['./device-demo.component.scss'],
})
export class DeviceDemoComponent {
  public position?: Position;

  constructor(public readonly device: Device, public readonly positionService: PositionService, private sanitizer: DomSanitizer) {
    positionService.positionChange.subscribe(position => (this.position = position));
  }

  public osmLink(): SafeResourceUrl {
    return this.sanitizer.bypassSecurityTrustResourceUrl(
      'https://www.openstreetmap.org/export/embed.html?bbox=9.6%2C56.1%2C9.7%2C56.2&amp;layer=mapnik&amp;marker=' +
      this.position?.longitude +
      '%2C' +
      this.position?.latitude
    );
  }
}
