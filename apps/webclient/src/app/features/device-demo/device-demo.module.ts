// Import Angular stuff

import { NgModule } from '@angular/core';

import { CdkTableModule } from '@angular/cdk/table';

// Import core functionality

import { CoreModule } from '@public/core';
import { LayoutModule } from '@public/core/ui/layout';

import { DeviceDemoRouting } from './device-demo.routing';

import { DeviceDemoComponent } from './components/device-demo.component';

@NgModule({
  imports: [
    CdkTableModule,

    CoreModule,
    LayoutModule,

    DeviceDemoRouting,
  ],
  declarations: [
    DeviceDemoComponent,
  ],
})
export class DeviceDemoModule {}
