// Import Angular stuff

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Import shared functionality, including Material Design and Flex Layout

import { DeviceDemoComponent } from './components/device-demo.component';

const routes: Routes = [
  {
    path: '',
    component: DeviceDemoComponent
  },
  {
    path: '**',
    component: DeviceDemoComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class DeviceDemoRouting {
}

