import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FormGroup, FormBuilder } from '@angular/forms';

import { ProtectedService } from '../services/protected.service';

@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  providers: [ProtectedService]
})
export class ProtectedEditComponent implements OnInit {
  public data: any = {};
  public form: FormGroup;

  public title = 'New Contact';

  constructor(
    private service: ProtectedService,
    private builder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.form = builder.group({
      name: [],
      title: [],
      phone: [],
      email: []
    });
  }

  public ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    if (id) {
      this.service.get(id).subscribe(data => {
        this.data = data;

        this.title =
          (data.title ? data.title + ' ' : ' ') +
          (data.name ?  data.name : '');

        this.form.setValue({
          name: data.name,
          title: data.title,
          phone: data.phone,
          email: data.email
        });
      });
    }
  }

  public save(): void {
    this.data.name = this.form.controls['name'].value;
    this.data.title = this.form.controls['title'].value;
    this.data.phone = this.form.controls['phone'].value;
    this.data.email = this.form.controls['email'].value;

    if (this.data.id) {
      this.service.put(this.data.id, this.data).subscribe(data => {
        this.router.navigateByUrl('/protected');
      });
    } else {
      this.service.post(this.data).subscribe(data => {
        this.router.navigateByUrl('/protected');
      });
    }
  }

  public delete(): void {
    if (this.data.id) {
      this.service.delete(this.data.id, this.data).subscribe(data => {
        this.router.navigateByUrl('/protected');
      });
    }
  }

  public cancel(): void {
    this.router.navigateByUrl('/protected');
  }
}
