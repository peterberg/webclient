import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

import { MatSort } from '@angular/material/sort';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { LocalStorage } from '@public/core';

import { ProtectedService } from '../services/protected.service';

const PAGE_SIZE_KEY = 'protected-list-page-size';

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  providers: [ProtectedService],
})
export class ProtectedListComponent implements AfterViewInit {
  @ViewChild(MatSort)
  public sort!: MatSort;

  @ViewChild(MatPaginator)
  public paginator!: MatPaginator;

  public dataSource!: MatTableDataSource<any>;
  public displayColumns = ['id', 'name', 'phone', 'email'];

  constructor(
    private readonly service: ProtectedService,
    private readonly router: Router,
    private readonly storage: LocalStorage
  ) {}

  public ngAfterViewInit() {
    this.service.search().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);

      this.paginator.pageSize = this.storage.get(PAGE_SIZE_KEY);

      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });

    this.paginator.page.subscribe((event: PageEvent) => {
      if (!!event.pageSize) {
        this.storage.set(PAGE_SIZE_KEY, event.pageSize);
      }
    });
  }

  public add() {
    this.router.navigateByUrl('/protected/edit');
  }
}
