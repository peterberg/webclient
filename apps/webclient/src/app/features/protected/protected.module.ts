// Import Angular stuff

import { NgModule } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { CdkTableModule } from '@angular/cdk/table';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatRippleModule } from '@angular/material/core';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';

// Import core functionality

import { CoreModule, UserService } from '@public/core';
import { LayoutModule } from '@public/core/ui/layout';

import { ProtectedRouting } from './protected.routing';

import { ProtectedListComponent } from './components/list.component';
import { ProtectedEditComponent } from './components/edit.component';

@NgModule({
  imports: [
    CdkTableModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatRippleModule,
    MatSortModule,
    MatTableModule,

    CoreModule,
    LayoutModule,

    ProtectedRouting,
  ],
  declarations: [ProtectedListComponent, ProtectedEditComponent],
})
export class ProtectedModule {
  constructor(userService: UserService, location: Location, router: Router) {
    // Subscribe to userChange events in order to redirect to the home page in case the user logs out.

    userService.userChange.subscribe(user => {
      if (!user && location.path().startsWith('/protected')) {
        router.navigateByUrl('/');
      }
    });
  }
}
