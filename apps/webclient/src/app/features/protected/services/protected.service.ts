// Import Angular stuff

import { Injectable } from '@angular/core';

// Import RxJS stuff

import { Observable } from 'rxjs';

import { HttpService } from '../../../services/http.service';

@Injectable({
  providedIn: 'root',
})
export class ProtectedService {
  private readonly PATH = '/contacts';

  constructor(private httpService: HttpService) {}

  public search(): Observable<any[]> {
    return this.httpService.search(this.PATH);
  }

  public get(key: string): Observable<any> {
    return this.httpService.get(this.PATH, key);
  }

  public post(payload: any): Observable<any> {
    return this.httpService.post(this.PATH, payload);
  }

  public put(key: string, payload: any): Observable<any> {
    return this.httpService.put(this.PATH, key, payload);
  }

  public delete(key: string, payload: any): Observable<any> {
    return this.httpService.delete(this.PATH, key, payload);
  }
}
