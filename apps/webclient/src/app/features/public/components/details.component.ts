import { Component, Input } from '@angular/core';

@Component({
  selector: 'list-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class ListDetailComponent {
  @Input()
  public details: any = {};
}
