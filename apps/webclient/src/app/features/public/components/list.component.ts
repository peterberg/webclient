import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { animate, state, style, transition, trigger } from '@angular/animations';

import { MatSort } from '@angular/material/sort';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { LocalStorage } from '@public/core';

import { PublicService } from '../services/public.service';

const PAGE_SIZE_KEY = 'public-list-page-size';

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  providers: [PublicService],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class PublicListComponent implements AfterViewInit {
  @ViewChild(MatSort)
  public sort!: MatSort;

  @ViewChild(MatPaginator)
  public paginator!: MatPaginator;

  public dataSource!: MatTableDataSource<any>;
  public displayColumns = ['id', 'author', 'title', 'year', 'pages', 'indicator'];
  public expandedRow: any | null;

  constructor(
    private readonly service: PublicService,
    private readonly router: Router,
    private readonly storage: LocalStorage
  ) {}

  public ngAfterViewInit() {
    this.service.search().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);

      this.paginator.pageSize = this.storage.get(PAGE_SIZE_KEY);

      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });

    this.paginator.page.subscribe((event: PageEvent) => {
      if (!!event.pageSize) {
        this.storage.set(PAGE_SIZE_KEY, event.pageSize);
      }
    });
  }

  public applyFilter(event: Event) {
    this.dataSource.filter = (event.target as HTMLInputElement).value.trim().toLowerCase();
  }

  public add() {
    this.router.navigateByUrl('/public/edit');
  }
}
