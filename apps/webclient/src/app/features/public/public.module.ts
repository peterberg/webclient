// Import Angular stuff

import { NgModule } from '@angular/core';

import { CdkTableModule } from '@angular/cdk/table';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatRippleModule } from '@angular/material/core';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';

// Import core functionality

import { CoreModule } from '@public/core';
import { LayoutModule } from '@public/core/ui/layout';

import { PublicRouting } from './public.routing';

import { PublicListComponent } from './components/list.component';
import { ListDetailComponent } from './components/details.component';

@NgModule({
  imports: [
    CdkTableModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatRippleModule,
    MatSortModule,
    MatTableModule,

    CoreModule,
    LayoutModule,

    PublicRouting,
  ],
  declarations: [PublicListComponent, ListDetailComponent],
})
export class PublicModule {}
