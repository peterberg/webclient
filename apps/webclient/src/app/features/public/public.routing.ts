// Import Angular stuff

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Import shared functionality, including Material Design and Flex Layout

import { PublicListComponent } from './components/list.component';

const routes: Routes = [
  {
    path: 'list',
    component: PublicListComponent
  },
  {
    path: '**',
    component: PublicListComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class PublicRouting {
}

