// Import Angular stuff

import { NgModule, APP_INITIALIZER } from '@angular/core';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';

import { of } from 'rxjs';

import { environment } from '../environments/environment';

import { Breakpoints } from '@angular/cdk/layout';

import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSliderModule } from '@angular/material/slider'

// Import core, application wide functionality.

import { CoreModule, RootComponent, ErrorInterceptor } from '@public/core';
import { LoginDialogConfig } from '@public/core/authentication';
import { OAuth2Config, AuthorizationInterceptor } from '@public/core/authorization';

import { LayoutModule } from '@public/core/ui/layout';

import { Toolbar } from '@public/core/ui/toolbar';
import { Sidenav, SIDENAV_CONFIG } from '@public/core/ui/sidenav';

// Import root routing definitions

import { RootRouting } from './root.routing';

// Import components which are eagerly loaded by the app

import {
  SidenavContentComponent,
  ToolbarContentComponent,
  HomeComponent,
  NotFoundComponent,
  PrivacyPolicyComponent,
  TermsOfUseComponent,
  AboutDialogComponent,
  CookieDialogComponent,
  ThemeToggleComponent
} from './components';

import {
  FacebookAuthenticationProvider,
  GoogleAuthenticationProvider,
  LinkedInAuthenticationProvider,
  AuthenticationConfig,
} from '@public/core/authentication';

// Setup external authentication configuration

const authenticationConfig: AuthenticationConfig = {
  providers: [
    new GoogleAuthenticationProvider('11339069686-v64gsia2q1iip6ka996gehkg1ala2g2t.apps.googleusercontent.com'),
    new FacebookAuthenticationProvider('277688382736354'),
    new LinkedInAuthenticationProvider('LinkedIn-client-Id', false, 'en_US')
  ]
}

// Provided value for LoginDialogConfig. This config object is identical to the default in LoginDialogComponent and thus
// redundant, but it is provided here to illustrate the use of configuration values.

const loginDialogConfig: LoginDialogConfig = {
  labels: {
    title: 'Login using',
    username: 'Username',
    password: 'Password',
    alternative: 'or',
    login: 'Login',
    cancel: 'Cancel',
  },
  messages: {
    loginSuccess: (name: string) => `You have been logged in as ${name}`,
    loginFailure: 'An error occurred while logging in',
    badCredentials: 'Login failed, due to wrong username or password',
  },
};

// Provided value for OAuth2Config. This config object is identical to the default in LoginDialogComponent and thus
// redundant, but it is provided here to illustrate the use of configuration values.

const oauth2Config: OAuth2Config = {
  path: '/api/tokens',
  auth: 'Basic ' + btoa('api' + ':' + 'secret'),
};

export function initSidenav(sidenav: Sidenav) {
  return () => {
    sidenav.content = SidenavContentComponent;
    return of(true).toPromise();
  };
}

export function initToolbar(toolbar: Toolbar) {
  return () => {
    toolbar.content = ToolbarContentComponent;
    return of(true).toPromise();
  };
}

@NgModule({
  imports: [
    HttpClientModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),

    MatButtonModule,
    MatDividerModule,
    MatDialogModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatSliderModule,

    CoreModule,
    LayoutModule,

    RootRouting,
  ],
  declarations: [
    HomeComponent,
    SidenavContentComponent,
    ToolbarContentComponent,
    NotFoundComponent,
    PrivacyPolicyComponent,
    TermsOfUseComponent,
    AboutDialogComponent,
    CookieDialogComponent,
    ThemeToggleComponent,
  ],
  providers: [
    {
      provide: AuthenticationConfig,
      useValue: authenticationConfig,
    },
    {
      provide: LoginDialogConfig,
      useValue: loginDialogConfig,
    },
    {
      provide: OAuth2Config,
      useValue: oauth2Config,
    },
    { provide: HTTP_INTERCEPTORS, useClass: AuthorizationInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

    { provide: APP_INITIALIZER, deps: [Sidenav], useFactory: initSidenav, multi: true },
    { provide: APP_INITIALIZER, deps: [Toolbar], useFactory: initToolbar, multi: true },

    { provide: SIDENAV_CONFIG, useValue: [Breakpoints.Handset, Breakpoints.Tablet, Breakpoints.Web] },
  ],
  bootstrap: [RootComponent],
})
export class RootModule {}
