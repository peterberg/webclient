// Import Angular stuff

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Imports from core library required by the root router

import { AuthenticationGuard } from '@public/core/authentication';

// Import components referenced by the root router (i.e. compoments which are actived through an eager loaded route).

import { HomeComponent, NotFoundComponent, PrivacyPolicyComponent, TermsOfUseComponent } from './components';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'index.html',
    component: HomeComponent,
  },
  {
    path: 'protected',
    canLoad: [AuthenticationGuard],
    loadChildren: () => import('./features/protected/protected.module').then(m => m.ProtectedModule),
  },
  {
    path: 'public',
    loadChildren: () => import('./features/public/public.module').then(m => m.PublicModule),
  },
  {
    path: 'charts',
    loadChildren: () => import('./features/charts-demo/charts-demo.module').then(m => m.ChartsDemoModule),
  },

  {
    path: 'device',
    loadChildren: () => import('./features/device-demo/device-demo.module').then(m => m.DeviceDemoModule),
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent,
  },
  {
    path: 'terms-of-use',
    component: TermsOfUseComponent,
  },
  {
    path: '**',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: true })],
})
export class RootRouting {}
