// Import Angular stuff

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  private readonly PATH_PREFIX = '/api';

  constructor(private httpClient: HttpClient) {}

  public search(path: string): Observable<any> {
    return this.httpClient.get(this.PATH_PREFIX + path);
  }

  public get(path: string, key: string): Observable<any> {
    return this.httpClient.get(this.PATH_PREFIX + path + '/' + key);
  }

  public post(path: string, payload: any): Observable<any> {
    return this.httpClient.post(this.PATH_PREFIX + path, payload);
  }

  public put(path: string, key: string, payload: any): Observable<any> {
    return this.httpClient.put(this.PATH_PREFIX + path + '/' + key, payload);
  }

  public delete(path: string, key: string, payload: any): Observable<any> {
    return this.httpClient.delete(this.PATH_PREFIX + path + '/' + key, payload);
  }
}
