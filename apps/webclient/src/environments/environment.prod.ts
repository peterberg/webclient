import pkg from '../../../../package-lock.json';

export const environment = {
  production: true,
  myVersion: pkg.version,
  ngVersion: pkg.dependencies['@angular/core'].version,
  cdkVersion: pkg.dependencies['@angular/cdk'].version,
  matVersion: pkg.dependencies['@angular/material'].version,
  rxVersion: pkg.dependencies['rxjs'].version,
  tsVersion: pkg.dependencies['typescript'].version
}; 
