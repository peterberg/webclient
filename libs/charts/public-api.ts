/**
 * Public surface API of chart library
 */

export { ChartsModule } from './src/charts.module';

export {
  BarChartDirective,
  LineChartDirective,
  PieChartDirective,
  RadarChartDirective,
  ScatterChartDirective,
} from './src/directives';
