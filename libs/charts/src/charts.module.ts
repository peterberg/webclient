import { NgModule } from '@angular/core';

import {
  BarChartDirective,
  LineChartDirective,
  PieChartDirective,
  RadarChartDirective,
  ScatterChartDirective,
} from './directives';

@NgModule({
  declarations: [BarChartDirective, LineChartDirective, PieChartDirective, RadarChartDirective, ScatterChartDirective],
  exports: [BarChartDirective, LineChartDirective, PieChartDirective, RadarChartDirective, ScatterChartDirective],
})
export class ChartsModule {}
