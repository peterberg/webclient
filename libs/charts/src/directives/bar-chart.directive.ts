import { Directive, Input, OnInit } from '@angular/core';

import { ChartBase } from './chart-base';

@Directive({
  selector: 'canvas[barChart]',
})
export class BarChartDirective extends ChartBase implements OnInit {
  public ngOnInit() {
    this.create('bar');
  }
}
