import { Directive, ElementRef, Input, NgZone } from '@angular/core';

import { Chart, ChartTypeRegistry, registerables } from 'chart.js';

Chart.register(...registerables);

export type ChartType = keyof ChartTypeRegistry;
@Directive()
export class ChartBase {
  @Input()
  public data: any;

  @Input()
  public options: any;

  private canvas: HTMLCanvasElement;

  private chart!: Chart;

  constructor(canvas: ElementRef, private readonly ngZone: NgZone) {
    this.canvas = canvas.nativeElement;
  }

  protected create(type: ChartType) {
    this.ngZone.runOutsideAngular(() => {
      // Create the chart itself using the canvas as context.

      this.chart = new Chart(this.canvas, {
        type,
        data: this.data,

        options: this.options
          ? this.options
          : {
              responsive: true,
              maintainAspectRatio: true,
            },
      });
    });
  }
}
