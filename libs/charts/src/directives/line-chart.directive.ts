import { Directive, OnInit } from '@angular/core';

import { ChartBase } from './chart-base';

@Directive({
  selector: 'canvas[lineChart]',
})
export class LineChartDirective extends ChartBase implements OnInit {
  public ngOnInit() {
    this.create('line');
  }
}
