import { Directive, OnInit } from '@angular/core';

import { ChartBase } from './chart-base';

@Directive({
  selector: 'canvas[pieChart]',
})
export class PieChartDirective extends ChartBase implements OnInit {
  public ngOnInit() {
    this.create('pie');
  }
}
