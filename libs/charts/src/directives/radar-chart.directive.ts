import { Directive, OnInit } from '@angular/core';

import { ChartBase } from './chart-base';

@Directive({
  selector: 'canvas[radarChart]',
})
export class RadarChartDirective extends ChartBase implements OnInit {
  public ngOnInit() {
    this.create('radar');
  }
}
