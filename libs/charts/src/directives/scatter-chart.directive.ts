import { Directive, OnInit } from '@angular/core';

import { ChartBase } from './chart-base';

@Directive({
  selector: 'canvas[scatterChart]',
})
export class ScatterChartDirective extends ChartBase implements OnInit {
  public ngOnInit() {
    this.create('scatter');
  }
}
