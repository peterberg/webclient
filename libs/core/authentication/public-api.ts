/**
 * Public surface API of @public/core/authentication
 */

export { AuthenticationModule } from './src/authentication.module';

export { LoginDialogComponent, LoginDialogConfig } from './src/components';

export {
  AuthenticationProvider,
  BaseAuthenticationProvider,
  FacebookAuthenticationProvider,
  GoogleAuthenticationProvider,
  LinkedInAuthenticationProvider,
} from './src/providers';

export { AuthenticationService, AuthenticationConfig } from './src/services';

export { AuthenticationGuard } from './src/guards'
