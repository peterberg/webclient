// Import Angular, CDK and Material stuff

import { NgModule, ModuleWithProviders } from '@angular/core';

import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';

// Import core functionality

import { CoreModule } from '@public/core';

// Import components, directives and pipes declared by this module

import { LoginDialogComponent } from './components';

// Import services up for configuration

import { AuthenticationService, AuthenticationConfig } from './services';

@NgModule({
  imports: [
    MatButtonModule,
    MatDialogModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,

    CoreModule
  ],
  declarations: [
    LoginDialogComponent
  ],
  exports: [
    LoginDialogComponent
  ]

})
export class AuthenticationModule {

  public static initialize(config: AuthenticationConfig): ModuleWithProviders<AuthenticationModule> {
    return {
      ngModule: AuthenticationModule,
      providers: [
        AuthenticationService,
        {
          provide: AuthenticationConfig,
          useValue: config
        }
      ]
    };
  }
}
