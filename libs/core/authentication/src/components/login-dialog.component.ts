// Import Angular stuff

import { Component, OnInit, Optional } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

import { MatDialogRef } from '@angular/material/dialog';

// Import RxJS stuff

import { first } from 'rxjs/operators';

// Imports form other @public/core entry points

import { AlertService, UserService } from '@public/core';

import { OAuth2Service } from '@public/core/authorization';

// Local imports

import { AuthenticationService } from '../services';
import {
  FacebookAuthenticationProvider,
  GoogleAuthenticationProvider,
  LinkedInAuthenticationProvider,
} from '../providers';

export class LoginDialogConfig {
  labels?: {
    title?: string;
    username?: string;
    password?: string;
    alternative?: string;
    login?: string;
    cancel?: string;
  };
  messages?: {
    loginSuccess?: string | ((name: string) => string);
    loginFailure?: string;
    badCredentials?: string;
  };
}

const defaultConfig: LoginDialogConfig = {
  labels: {
    title: 'Login using',
    username: 'Username',
    password: 'Password',
    alternative: 'or',
    login: 'Login',
    cancel: 'Cancel',
  },
  messages: {
    loginSuccess: (name: string) => `You have been logged in as ${name}`,
    loginFailure: 'An error occurred while logging in',
    badCredentials: 'Login failed, due to wrong username or password',
  },
};

@Component({
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.scss'],
})
export class LoginDialogComponent implements OnInit {
  public form!: FormGroup;

  public username = new FormControl<string>('', Validators.required);
  public password = new FormControl<string>('', Validators.required);

  public facebook = false;
  public google = false;
  public linkedIn = false;

  public message?: string;

  public readonly config: LoginDialogConfig;

  constructor(
    private authenticationService: AuthenticationService,
    private oauth2Service: OAuth2Service,
    private alertService: AlertService,
    private userService: UserService,
    private builder: FormBuilder,
    private dialog: MatDialogRef<LoginDialogComponent>,
    @Optional() config: LoginDialogConfig
  ) {
    this.config = {
      labels: { ...defaultConfig.labels, ...config?.labels },
      messages: { ...defaultConfig.messages, ...config?.messages },
    };
  }

  public ngOnInit() {
    this.form = this.builder.group({
      username: this.username,
      password: this.password,
    });

    this.authenticationService.providers.forEach(provider => {
      if (provider instanceof FacebookAuthenticationProvider) {
        this.facebook = true;
      }
      if (provider instanceof GoogleAuthenticationProvider) {
        this.google = true;
      }
      if (provider instanceof LinkedInAuthenticationProvider) {
        this.linkedIn = true;
      }
    });
  }

  public login() {
    this.message = undefined;

    this.oauth2Service
      .login(this.username.value, this.password.value)
      .subscribe(this.onLoginSuccess, this.onLoginFailure);
  }

  public cancel() {
    this.dialog.close();
  }

  public loginWithFacebook(): void {
    this.dialog.close();
    const provider = this.authenticationService.providers.find(
      provider => provider instanceof FacebookAuthenticationProvider
    );

    if (!!provider) {
      this.authenticationService.login(provider).then(response => {
        this.userService.setCurrentUser({ realname: response.name });

        const message =
          typeof this.config.messages?.loginSuccess === 'function'
            ? this.config.messages.loginSuccess(response.name)
            : this.config.messages?.loginSuccess;

        if (!!message) {
          this.alertService.success(message, 3000);
        }
      });
    }
  }

  public loginWithGoogle(): void {
    this.dialog.close();

    this.alertService.warning('Login with Google has not been fully implemented');

    const provider = this.authenticationService.providers.find(
      provider => provider instanceof GoogleAuthenticationProvider
    );

    if (!!provider) {
      this.authenticationService.login(provider).then(this.onLoginSuccess);
    }
  }

  public loginWithLinkedin(): void {
    this.dialog.close();

    this.alertService.warning('Login with Linkedin has not been fully implemented');

    const provider = this.authenticationService.providers.find(
      provider => provider instanceof LinkedInAuthenticationProvider
    );

    if (!!provider) {
      this.authenticationService.login(provider).then(this.onLoginSuccess);
    }
  }

  private onLoginSuccess = () => {
    this.dialog.close();

    this.userService.userChange.pipe(first()).subscribe((user: any) => {
      const message =
        typeof this.config.messages?.loginSuccess === 'function'
          ? this.config.messages.loginSuccess(user.realname)
          : this.config.messages?.loginSuccess;

      if (!!message) {
        this.alertService.success(message, 3000);
      }
    });
  };

  private onLoginFailure = (error: any) => {
    if (error.status === 400) {
      this.message = this.config.messages?.badCredentials;
    } else {
      this.dialog.close();
      if (!!this.config.messages?.loginFailure) {
        this.alertService.error(this.config.messages.loginFailure);
      }
    }
  };
}
