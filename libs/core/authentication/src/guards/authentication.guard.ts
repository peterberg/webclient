import { Injectable, Optional, SkipSelf } from '@angular/core';
import { CanActivate, CanLoad } from '@angular/router';

import { MatDialog } from '@angular/material/dialog';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { UserService,SingletonError } from '@public/core';

import { LoginDialogComponent } from '../components';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationGuard implements CanActivate, CanLoad {
  constructor(
    private dialog: MatDialog,
    private userService: UserService,
    @Optional() @SkipSelf() readonly instance: AuthenticationGuard
  ) {
    if (instance) {
      throw new SingletonError(this);
    }
  }

  public canActivate(): boolean | Observable<boolean> {
    return this.ensureAuthentication();
  }

  public canLoad(): boolean | Observable<boolean> {
    return this.ensureAuthentication();
  }

  private ensureAuthentication(): boolean | Observable<boolean> {
    if (!this.userService.getCurrentUser()) {
      this.dialog.open(LoginDialogComponent, { disableClose: true });

      return this.userService.userChange.pipe(map(user => !!user));
    }

    return true;
  }
}
