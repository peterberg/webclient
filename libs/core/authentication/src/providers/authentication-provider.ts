export interface AuthenticationProvider {
  name: string;

  initialize(): Promise<void>;
  getStatus(): Promise<any>;
  login(config?: any): Promise<any>;
  logout(revoke?: boolean): Promise<any>;
}
