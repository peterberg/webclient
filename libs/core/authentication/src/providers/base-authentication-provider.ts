import { BehaviorSubject } from 'rxjs';

import { AuthenticationProvider } from './authentication-provider';

export abstract class BaseAuthenticationProvider implements AuthenticationProvider {
  protected _readyState: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(public readonly name: string) {}

  protected onReady(): Promise<void> {
    return new Promise((resolve, reject) => {
      this._readyState.subscribe((isReady: boolean) => {
        if (isReady) {
          resolve();
        }
      });
    });
  }

  abstract initialize(): Promise<void>;
  abstract getStatus(): Promise<any>;
  abstract login(): Promise<any>;
  abstract logout(revoke?: boolean): Promise<any>;

  public loadScript(src: string, onload: any, async = true, text = ''): void {
    if (document.getElementById(this.name)) {
      return;
    }

    const signInJS = document.createElement('script');

    signInJS.async = async;
    signInJS.src = src;
    signInJS.onload = onload;
    signInJS.text = text; // LinkedIn
    document.head.appendChild(signInJS);
  }
}
