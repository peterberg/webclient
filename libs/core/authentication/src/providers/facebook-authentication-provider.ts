import { BaseAuthenticationProvider } from './base-authentication-provider';

declare let FB: any;

export class FacebookAuthenticationProvider extends BaseAuthenticationProvider {
  constructor(
    private client: string,
    private locale: string = 'en_US',
    private config: any = { scope: 'email,public_profile' },
    private fields: string = 'name,email',
    private version: string = 'v10.0'
  ) {
    super('Facebook');
  }

  public initialize(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.loadScript(
        `//connect.facebook.net/${this.locale}/sdk.js`,
        () => {
          FB.init({
            appId: this.client,
            autoLogAppEvents: true,
            cookie: true,
            xfbml: true,
            version: this.version
          });
          // FB.AppEvents.logPageView(); #FIX for #18

          this._readyState.next(true);
          resolve();
        }
      );
    });
  }

  public getStatus(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.onReady().then(() => {
        FB.getAuthenticationStatus((response: any) => {
          if (response.status === 'connected') {
            const authResponse = response.authResponse;
            FB.api(`/me?fields=${this.fields}`, (fbUser: any) => {
              const user: any = {};

              user.id = fbUser.id;
              user.name = fbUser.name;
              user.email = fbUser.email;
              user.photoUrl =
                'https://graph.facebook.com/' +
                fbUser.id +
                '/picture?type=normal';
              user.authToken = authResponse.accessToken;

              resolve(user);
            });
          }
        });
      });
    });
  }

  public login(config?: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.onReady().then(() => {
        FB.login((response: any) => {
          if (response.authResponse) {
            const authResponse = response.authResponse;
            FB.api(`/me?fields=${this.fields}`, (fbUser: any) => {
              const user: any = {};

              user.id = fbUser.id;
              user.name = fbUser.name;
              user.email = fbUser.email;
              user.photoUrl =
                'https://graph.facebook.com/' +
                fbUser.id +
                '/picture?type=normal';
              user.authToken = authResponse.accessToken;

              resolve(user);
            });
          } else {
            reject('User cancelled authentication or did not fully authorize.');
          }
        }, this.config);
      });
    });
  }

  public logout(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.onReady().then(() => {
        FB.logout((response: any) => {
          resolve();
        });
      });
    });
  }
}
