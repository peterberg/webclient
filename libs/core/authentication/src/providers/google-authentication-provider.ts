import { BaseAuthenticationProvider } from './base-authentication-provider';

declare let gapi: any;

export class GoogleAuthenticationProvider extends BaseAuthenticationProvider {
  protected auth2: any;

  constructor(
    private clientId: string,
    private config: any = { scope: 'email' }
  ) {
    super('Google');
  }

  public initialize(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.loadScript(
        'https://apis.google.com/js/platform.js',
        () => {
          gapi.load('auth2', () => {
            this.auth2 = gapi.auth2.init({
              ...this.config,
              client_id: this.clientId
            });

            this.auth2
              .then(() => {
                this._readyState.next(true);
                resolve();
              })
              .catch((err: any) => {
                reject(err);
              });
          });
        }
      );
    });
  }

  public getStatus(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.onReady().then(() => {
        if (this.auth2.isSignedIn.get()) {
          const user: any = {};

          const profile = this.auth2.currentUser.get().getBasicProfile();
          const token = this.auth2.currentUser.get().getAuthResponse(true)
            .access_token;
          const backendToken = this.auth2.currentUser
            .get()
            .getAuthResponse(true).id_token;

          user.id = profile.getId();
          user.name = profile.getName();
          user.email = profile.getEmail();
          user.photoUrl = profile.getImageUrl();
          user.firstName = profile.getGivenName();
          user.lastName = profile.getFamilyName();
          user.authToken = token;
          user.idToken = backendToken;

          resolve(user);
        }
      });
    });
  }

  login(config?: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.onReady().then(() => {
        const promise = this.auth2.signIn(config);

        promise
          .then(
            () => {
              const user: any = {};

              const profile = this.auth2.currentUser.get().getBasicProfile();
              const token = this.auth2.currentUser.get().getAuthResponse(true)
                .access_token;
              const backendToken = this.auth2.currentUser
                .get()
                .getAuthResponse(true).id_token;

              user.id = profile.getId();
              user.name = profile.getName();
              user.email = profile.getEmail();
              user.photoUrl = profile.getImageUrl();
              user.firstName = profile.getGivenName();
              user.lastName = profile.getFamilyName();
              user.authToken = token;
              user.idToken = backendToken;

              resolve(user);
            },
            (closed: any) => {
              reject('User cancelled authentication or did not fully authorize.');
            }
          )
          .catch((err: any) => {
            reject(err);
          });
      });
    });
  }

  logout(revoke?: boolean): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.onReady().then(() => {
        let signOutPromise: Promise<any>;

        if (revoke) {
          signOutPromise = this.auth2.disconnect();
        } else {
          signOutPromise = this.auth2.signOut();
        }

        signOutPromise
          .then((error: any) => {
            if (error) {
              reject(error);
            } else {
              resolve();
            }
          })
          .catch((error: any) => {
            reject(error);
          });
      });
    });
  }
}
