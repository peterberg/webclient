import { BaseAuthenticationProvider } from './base-authentication-provider';

declare let IN: any;

/*
 *
 */
export class LinkedInAuthenticationProvider extends BaseAuthenticationProvider {
  constructor(
    private clientId: string,
    private authorize?: boolean,
    private locale?: string,
    private fields: string = 'id,first-name,last-name,email-address,picture-url'
  ) {
    super('LinkedIn');
  }

  public initialize(): Promise<void> {
    let text = '';

    text += 'api_key: ' + this.clientId + '\r\n';
    text += 'authorize:' + (this.authorize ? 'true' : 'false') + '\r\n';
    text += 'lang: ' + (this.locale ? this.locale : 'en_US') + '\r\n';

    return new Promise<void>((resolve, reject) => {
      this.loadScript(
        '//platform.linkedin.com/in.js',
        () => {
          const that = this;
          setTimeout(() => {
            this._readyState.next(true);
            resolve();
          }, 800);
        },
        false,
        text
      );
    });
  }

  public getStatus(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.onReady().then(() => {
        if (IN.User.isAuthorized()) {
          IN.API.Raw(`/people/~:(${this.fields})`).result((res: any) => {
            const user: any = {};

            user.id = res.id;
            user.name = res.firstName + ' ' + res.lastName;
            user.email = res.emailAddress;
            user.photoUrl = res.pictureUrl;
            user.authToken = IN.ENV.auth.oauth_token;

            resolve(user);
          });
        }
      });
    });
  }

  public login(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.onReady().then(() => {
        IN.User.authorize(() => {
          IN.API.Raw(`/people/~:(${this.fields})`).result((res: any) => {
            const user: any = {};

            user.id = res.id;
            user.name = res.firstName + ' ' + res.lastName;
            user.email = res.emailAddress;
            user.photoUrl = res.pictureUrl;
            user.authToken = IN.ENV.auth.oauth_token;

            resolve(user);
          });
        });
      });
    });
  }

  public logout(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.onReady().then(() => {
        IN.User.logout(() => {
          resolve();
        }, {});
      });
    });
  }
}
