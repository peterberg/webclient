import { Injectable, Optional, SkipSelf } from '@angular/core';

import { Observable, BehaviorSubject, ReplaySubject } from 'rxjs';

import { AuthenticationProvider } from '../providers';

export class AuthenticationConfig {
  providers?: AuthenticationProvider[];
}

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  public static readonly ERR_PROVIDER_NOT_FOUND = 'Authentication provider not found';
  public static readonly ERR_NOT_AUTHENTICATED = 'Not authenticated';

  private _providers: AuthenticationProvider[];

  private _user: any = null;
  private _authState: ReplaySubject<any> = new ReplaySubject(1);
  private _readyState: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);

  get authState(): Observable<any> {
    return this._authState.asObservable();
  }

  get readyState(): Observable<string[]> {
    return this._readyState.asObservable();
  }

  get providers(): AuthenticationProvider[] {
    return this._providers;
  }

  constructor(config: AuthenticationConfig, @Optional() @SkipSelf() readonly instance: AuthenticationService) {
    if (instance) {
      throw new Error(`${this.constructor.name} already instantiated. Provide it only in the root module.`);
    }

    this._providers = config.providers ? config.providers : [];
    
    Object.entries(this._providers).forEach(([key, provider]) => {
      provider
        .initialize()
        .then(() => {
          const readyProviders = this._readyState.getValue();
          readyProviders.push(key);
          this._readyState.next(readyProviders);

          provider.getStatus().then(user => {
            user.provider = key;

            this._user = user;
            this._authState.next(user);
          });
        })
        .catch(_ => {
          this._authState.next(undefined);
        });
    });
  }

  public login(provider: AuthenticationProvider, config?: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      provider
        .login(config)
        .then((user: any) => {
          user.provider = provider.name;
          resolve(user);

          this._user = user;
          this._authState.next(user);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public logout(revoke: boolean = false): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      if (!this._user) {
        reject(AuthenticationService.ERR_NOT_AUTHENTICATED);
      } else {
        const providerId = this._user.provider;
        const providerObject = this.providers[providerId];
        if (providerObject) {
          providerObject
            .logout(revoke)
            .then(() => {
              resolve();

              this._user = null;
              this._authState.next(undefined);
            })
            .catch(err => {
              reject(err);
            });
        } else {
          reject(AuthenticationService.ERR_PROVIDER_NOT_FOUND);
        }
      }
    });
  }
}
