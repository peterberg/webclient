/**
 * Public surface API of @public/core/authorization
 */

export { AuthorizationModule } from './src/authorization.module';

export { OAuth2Service, OAuth2Config } from './src/services';

export { AuthorizationInterceptor } from './src/interceptors';
