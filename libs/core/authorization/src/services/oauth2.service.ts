// Import Angular stuff

import { Injectable, Optional } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

// Import RxJS stuff

import { Observable, of, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';

// Import services used by the OAuth2Service

import { UserService, TokenService } from '@public/core';

export class OAuth2Config {
  path = '/api/tokens';
  auth = 'Basic ' + btoa('api' + ':' + 'secret');
}

@Injectable({
  providedIn: 'root',
})
export class OAuth2Service {
  private loginSource = new Subject<any>();
  private logoutSource = new Subject<void>();

  private readonly options: any;

  constructor(
    private readonly userService: UserService,
    private readonly tokenService: TokenService,
    private readonly httpClient: HttpClient,
    @Optional() private readonly config: OAuth2Config
  ) {
    // Use default config unless one has been provided.

    if (!this.config) {
      this.config = new OAuth2Config();
    }

    this.options = {
      headers: new HttpHeaders({
        Authorization: this.config.auth,
        'Content-Type': 'application/x-www-form-urlencoded',
      }),
    };
  }

  public onLogin = this.loginSource.asObservable();
  public onLogout = this.logoutSource.asObservable();

  /**
   *
   */

  public login(username: string | null, password: string | null): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
      }),
    };

    return this.httpClient
      .post(this.config.path, 'grant_type=password&username=' + username + '&password=' + password, options)
      .pipe(tap(this.onLoginSuccess, this.onLoginFailure));
  }

  /**
   *
   */

  public logout(): Observable<any> {
    if (!!this.tokenService.getAccessToken()) {
      return this.httpClient
        .delete(this.config.path + '/' + this.tokenService.getAccessToken())
        .pipe(tap(this.onLogoutSuccess, this.onLogoutFailure));
    } else {
      this.onLogoutSuccess();
      return of(undefined);
    }
  }

  /**
   *
   */

  public refreshToken() {
    return this.httpClient
      .post(
        this.config.path,
        'grant_type=refresh_token&refresh_token=' + this.tokenService.getRefreshToken(),
        this.options
      )
      .pipe(tap(this.onRefreshSuccess, this.onRefreshFailure));
  }

  /**
   *
   */

  private onLoginSuccess = (response: any) => {
    this.tokenService.setAccessToken(response.access_token);
    this.tokenService.setRefreshToken(response.refresh_token);

    this.httpClient.get('/api/users/me').subscribe((me: any) => {
      this.userService.setCurrentUser(me);
      this.loginSource.next(me);
    });
  };

  /**
   *
   */

  private onLoginFailure = () => {
    this.tokenService.setAccessToken(undefined);
    this.tokenService.setRefreshToken(undefined);

    this.userService.setCurrentUser(undefined);
  };

  /**
   *
   */

  private onLogoutSuccess = () => {
    this.tokenService.setAccessToken(undefined);
    this.tokenService.setRefreshToken(undefined);

    this.userService.setCurrentUser(undefined);
    this.logoutSource.next();
  };

  /**
   *
   */

  private onLogoutFailure = () => {
    this.tokenService.setAccessToken(undefined);
    this.tokenService.setRefreshToken(undefined);

    this.userService.setCurrentUser(undefined);
  };

  /**
   *
   */

  private onRefreshSuccess = (response: any) => {
    this.tokenService.setAccessToken(response.access_token);
    this.tokenService.setRefreshToken(response.refresh_token);
  };

  /**
   *
   */

  private onRefreshFailure = () => {
    this.tokenService.setAccessToken(undefined);
    this.tokenService.setRefreshToken(undefined);

    this.userService.setCurrentUser(undefined);
  };
}
