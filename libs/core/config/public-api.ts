/**
 * Public surface API of @public/core/config
 */

export { ConfigService } from './src/services';
