import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ConfigService {
  constructor(private readonly httpClient: HttpClient) {}

  public get(file: string): Observable<any> {
      return this.httpClient.get('config/' + file + '.json');
  }
}
