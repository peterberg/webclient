/**
 * Public surface API of @public/core/i18n
 */

export { I18nService, TranslationService } from './src/services';
