// Import Angular stuff

import { NgModule } from '@angular/core';

// Import core functionality

import { CoreModule } from '@public/core';

// Import components, directives and pipes declared by this module

import { TranslationPipe } from './pipes';

@NgModule({
  imports: [
    CoreModule,

    TranslationPipe
  ],
  declarations: [
    TranslationPipe
  ]
})
export class I18nModule {
}
