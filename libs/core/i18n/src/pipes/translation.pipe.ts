import { Pipe, PipeTransform }from '@angular/core';

import { TranslationService } from '../services';

@Pipe({name: 'translate'})
export class TranslationPipe implements PipeTransform {

  constructor(private readonly translationService: TranslationService) {
  }

  public transform(value: string): string {
    return this.translationService.translate(value);
  }
}
