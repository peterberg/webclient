import { Injectable, InjectionToken, Inject, Optional } from '@angular/core';

import { Observable, BehaviorSubject } from 'rxjs';

import { SessionStorage } from '@public/core';

const LANGUAGE_KEY = 'language';
const DIRECTION_KEY = 'direction';

export const DEFAULT_LANGUAGE = new InjectionToken<string>('default-language');

export type Language = string;
export type Direction = 'ltr' | 'rtl';

@Injectable({
  providedIn: 'root',
})
export class I18nService {
  private languageChangeSource = new BehaviorSubject<string | undefined>(undefined);
  private directionChangeSource = new BehaviorSubject<Direction | undefined>(undefined);

  constructor(
    private readonly storage: SessionStorage,
    @Inject(DEFAULT_LANGUAGE) @Optional() defaultLanguage: Language
  ) {
    // Initialize language. Direction will be initialized indirectly through the language.

    if (!this.storage.get(LANGUAGE_KEY)) {
      this.storage.set(LANGUAGE_KEY, defaultLanguage ? defaultLanguage : this.getPlatformLanguage());
    }

    this.language = this.storage.get(LANGUAGE_KEY);
  }

  public get language(): string | undefined{
    return this.languageChangeSource.value;
  }

  public set language(value: string | undefined) {
    if (this.storage.get(LANGUAGE_KEY) !== value) {
      this.storage.set(LANGUAGE_KEY, value);
      this.languageChangeSource.next(value);

      // Set direction based on language. RTL for Arabic languages, LTR for all others.

      value?.startsWith('ar') ? (this.direction = 'rtl') : (this.direction = 'ltr');
    }
  }

  public get languageChange(): Observable<string | undefined> {
    return this.languageChangeSource.asObservable();
  }

  public get direction(): Direction | undefined {
    return this.directionChangeSource.value;
  }

  public set direction(value: Direction | undefined) {
    if (this.storage.get(DIRECTION_KEY) !== value) {
      this.storage.set(DIRECTION_KEY, value);
      this.directionChangeSource.next(value);
    }
  }

  public get directionChange(): Observable<Direction | undefined> {
    return this.directionChangeSource.asObservable();
  }

  private getPlatformLanguage(): Language | undefined {
    if (typeof window === 'undefined' || typeof window.navigator === 'undefined') {
      return undefined;
    }

    let language = window.navigator.languages ? window.navigator.languages[0] : undefined;

    language =
      language ||
      window.navigator.language ||
      (window.navigator as any).browserLanguage ||
      (window.navigator as any).browserLanguage;

    if (language) {
      language = language.replace('-', '_');
    }

    return language;
  }
}
