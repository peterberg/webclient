import { Injectable} from '@angular/core';
import { I18nService,Language } from './i18n.service';

import { ConfigService } from '@public/core/config';
import { Observable, Observer, AsyncSubject,of, forkJoin, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TranslationService {
  private config = new Map<string,string[]>();
  private dictionary = new Map<string,any>()

  private loading = new Map<string,boolean>();
  private featureLoaded = new Map<string,AsyncSubject<any>>();

  private translationChangeSource = new BehaviorSubject<Language | undefined>(undefined);

  constructor(private readonly configService: ConfigService, readonly i18nService: I18nService) {
    i18nService.languageChange.subscribe(this.onLanguageChange);
  }

  public get translationChange(): Observable<Language | undefined> {
    return this.translationChangeSource.asObservable();
  }

  public register(feature: string, files: string[]) {
    this.config.set(feature,files);
  }

  public translate(value: string, feature: string = 'shell'): Observable<string> {

    if(!value || value.length === 0 || !this.config.get(feature)) {
      return of(value);
    }

    if(!this.dictionary.get(feature)) {
      return this.loadAndTranslate(feature, value);
    }

    return of(this.getTranslation(feature,value) || value)
  }


  /**
   * Load translation files for a specified feature and language using the generic config service.
   * 
   * @param feature Feature to load file(s) for.
   * @param language The language to load.
   */

  private loadTranslations(feature: string, language: string): Observable<any[]> {
    const responses: Observable<any>[] = [];
    const path = (feature ==='shell') ? `/i18n/${language}` : `${feature}/i18n/${language}`;

    this.config.get(feature)?.forEach(file => {
      responses.push(this.configService.get(`${path}/${file}`));
    })

    return forkJoin(responses);
  }

  private loadAndTranslate(feature: string, value: string): Observable<string> {
    return of();
  }

  private getTranslation(feature: string, value: string): string | undefined {
    if(!this.dictionary.get(feature)) {
      return undefined;
    }

    return this.dictionary.get(feature).hasOwnProperty(value) ? this.dictionary.get(feature)[value] : value
  }

  
  private onLanguageChange = (language: Language | undefined) => {
    this.dictionary = new Map();

    this.translationChangeSource.next(language)
  }

}
