/**
 * Public surface API of @public/core/pwa
 */

export { PwaService } from './src/pwa.service';
