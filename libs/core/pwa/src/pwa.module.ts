// Import Angular, CDK and Material stuff

import { NgModule } from '@angular/core';

import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';

// Import core functionality

import { CoreModule } from '@public/core';

// Import components, directives and pipes declared by this module

import { InstallPromptComponent } from './install-prompt.component';

@NgModule({
  imports: [
    MatButtonModule,
    MatDialogModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,

    CoreModule
  ],
  declarations: [
    InstallPromptComponent
  ],
  exports: [
    InstallPromptComponent
  ]

})
export class PwaModule {
}
