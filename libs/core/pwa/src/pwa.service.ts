import { Injectable } from '@angular/core';
import { SwUpdate, VersionEvent } from '@angular/service-worker';

import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class PwaService {
  constructor(private readonly snackBar: MatSnackBar, readonly swUpdate: SwUpdate) {
    window.addEventListener('beforeinstallprompt', event => {
      // Prevent the mini-infobar from appearing on mobile
  
      event.preventDefault();

      // Stash the event so it can be triggered later.
 
      const deferredPrompt = event as any;
 
      const snackBarRef = this.snackBar.open('Want to install', 'Install');

      snackBarRef.onAction().subscribe(() => {  
        snackBarRef.dismiss();
        deferredPrompt.prompt();
  
        deferredPrompt.userChoice.then((result: any) => {
          console.log(result);
        });
      });
    });

    swUpdate.versionUpdates.subscribe((event: VersionEvent) => {
      const snackBarRef = this.snackBar.open(`A new version is available`, 'Update');

      snackBarRef.onAction().subscribe(() => {
        snackBarRef.dismiss();
        window.location.reload();
      });
    });
  }
}
