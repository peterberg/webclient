// Import Angular stuff

import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';

import { MatSidenav } from '@angular/material/sidenav';
import { MatToolbar } from '@angular/material/toolbar';

import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';

// Import RxJS stuff

import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Sidenav, SidenavMode } from '@public/core/ui/sidenav';

const sidenavOver = [Breakpoints.Handset, Breakpoints.TabletPortrait];

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.scss'],
})
export class RootComponent implements OnInit, OnDestroy {
  @ViewChild('sidenav', { static: true })
  public matSidenav!: MatSidenav;
  @ViewChild('toolbar', { static: true })
  public matMoolbar!: MatToolbar;

  private destroyer: ReplaySubject<boolean> = new ReplaySubject(1);

  constructor(private sidenav: Sidenav) {}

  /**
   *
   */

  public ngOnInit(): void {
    this.sidenav.stateChange.pipe(takeUntil(this.destroyer)).subscribe(state => {
      if (!!this.matSidenav) {
        if (state === 'open') {
          this.matSidenav.open();
        } else {
          this.matSidenav.close();
        }
      }
    });

    this.sidenav.modeChange.pipe(takeUntil(this.destroyer)).subscribe(mode => {
      if (!!this.matSidenav) {
        if (!!mode) {
          this.matSidenav.mode = mode;
        }
      }
    });
  }

  /**
   *
   */

  public ngOnDestroy(): void {
    this.destroyer.next(true);
    this.destroyer.complete();
  }
}
