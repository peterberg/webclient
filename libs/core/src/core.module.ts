// Import Angular, CDK and Marerial stuff

import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { ToolbarModule } from '@public/core/ui/toolbar';
import { SidenavModule } from '@public/core/ui/sidenav';

// Import components, directives and pipes

import { RootComponent } from './components';

import { DateTimePipe } from './pipes';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,

    MatSidenavModule,
    MatToolbarModule,
    MatSnackBarModule,

    ToolbarModule,
    SidenavModule,
  ],
  declarations: [RootComponent, DateTimePipe],
  exports: [CommonModule, RouterModule, FormsModule, ReactiveFormsModule, RootComponent, DateTimePipe],
})
export class CoreModule {}
