export class SingletonError extends Error {
  constructor(instance: object) {
    super(
      `${instance.constructor.name} already instantiated. Provide it only in the root module in order to ensure it is a singeton.`
    );
  }
}
