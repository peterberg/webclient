import { Pipe, PipeTransform } from '@angular/core';

import { DateTimeService } from '../services';

@Pipe({ name: 'dateTime' })
export class DateTimePipe implements PipeTransform {
  constructor(private readonly dateTimeService: DateTimeService) {}

  public transform(value?: number): string | undefined {
    return this.dateTimeService.format(value);
  }
}
