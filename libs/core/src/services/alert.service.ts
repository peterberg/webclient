// Import Angular stuff

import { Injectable, Optional, SkipSelf } from '@angular/core';

import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material/snack-bar';

import { SingletonError } from '../errors';

/**
 *
 */

@Injectable({
  providedIn: 'root',
})
export class AlertService {
  constructor(private readonly snackBar: MatSnackBar, @Optional() @SkipSelf() readonly instance: AlertService) {
    if (instance) {
      throw new SingletonError(this);
    }
  }

  /**
   *
   * @returns MatSnackBarRef<SimpleSnackBar>
   */

  public success(message: string, timeout?: number, acknowledge?: boolean | string): MatSnackBarRef<SimpleSnackBar> {
    const config: any = { panelClass: 'success' };

    config.duration = timeout ? timeout : undefined;

    return this.open(message, config, acknowledge);
  }

  /**
   *
   * @returns MatSnackBarRef<SimpleSnackBar>
   */

  public info(message: string, timeout?: number, acknowledge?: boolean | string): MatSnackBarRef<SimpleSnackBar> {
    const config: any = { panelClass: 'info' };

    config.duration = timeout ? timeout : undefined;

    return this.open(message, config, acknowledge);
  }

  /**
   *
   * @returns MatSnackBarRef<SimpleSnackBar>
   */

  public warning(message: string, timeout?: number, acknowledge?: boolean | string): MatSnackBarRef<SimpleSnackBar> {
    const config: any = { panelClass: 'warning' };

    config.duration = timeout ? timeout : undefined;

    return this.open(message, config, acknowledge);
  }

  /**
   *
   * @returns MatSnackBarRef<SimpleSnackBar>
   */

  public error(message: string, timeout?: number, acknowledge?: boolean | string): MatSnackBarRef<SimpleSnackBar> {
    const config: any = { panelClass: 'error' };

    config.duration = timeout ? timeout : undefined;

    return this.open(message, config, acknowledge);
  }

  /**
   *
   * @param message
   * @param config
   * @param acknowledge
   */

  private open(message: string, config: any, acknowledge?: boolean | string) {
    return this.snackBar.open(
      message,
      acknowledge ? (typeof acknowledge === 'string' ? acknowledge : 'OK') : config.timeout ? '' : 'OK',
      config
    );
  }
}
