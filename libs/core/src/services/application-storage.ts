// Import Angular stuff

import { Injectable,Optional,SkipSelf } from '@angular/core';

import { Storage } from './storage';

import { SingletonError } from '../errors';

@Injectable({
  providedIn: 'root',
})
export class ApplicationStorage implements Storage {
  private static readonly store = new Map<string,any>();

  constructor(@Optional() @SkipSelf() readonly instance: ApplicationStorage) {
    if (instance) {
      throw new SingletonError(instance);
    }
  }

  public get(key: string): any {
    return ApplicationStorage.store.get(key);
  }

  public set(key: string, value: any): void {

    // Remove the item from the store map if the value is null or undefined.

    if (!value) {
      ApplicationStorage.store.delete(key);
      return;
    }

    ApplicationStorage.store.set(key,value);
  }
}
