import { Injectable, Optional, SkipSelf } from '@angular/core';

import { SingletonError } from '../errors';

@Injectable({
  providedIn: 'root',
})
export class DateTimeService {
  constructor(@Optional() @SkipSelf() readonly instance: DateTimeService) {
    if (instance) {
      throw new SingletonError(this);
      
    }
  }
  
  public format(value?: number): string | undefined {
    return value ? new Date(value).toISOString() : undefined;
  }

  public parse(value?: string): number | undefined {
    return value ? Date.parse(value) : undefined;
  }
}
