// Import Angular stuff

import { Injectable,Optional,SkipSelf } from '@angular/core';

import { Storage } from './storage';

import { SingletonError } from '../errors';

@Injectable({
  providedIn: 'root',
})
export class LocalStorage implements Storage {
  constructor(@Optional() @SkipSelf() readonly instance: LocalStorage) {
    if (instance) {
      throw new SingletonError(instance);
    }
  }

  public get(key: string): any {
    const raw = window.localStorage.getItem(key);
    return raw ? JSON.parse(raw) : null;
  }

  public set(key: string, value: any): void {

    // Remove the item from the browser's local storage if value is null or
    // undefined.

    if (!value) {
      window.localStorage.removeItem(key);
      return;
    }

    window.localStorage.setItem(key, JSON.stringify(value));
  }
}
