import { Injectable, Optional, SkipSelf } from '@angular/core';

import { Observable, BehaviorSubject } from 'rxjs';

import { SingletonError } from '../errors';

export type PositionState = 'AVAILABLE' | 'UNAVAILABLE' | 'BLOCKED' | 'UNSUPPORTED';

export interface Position {
  latitude: number;
  longitude: number;
  altitude?: number;
  heading?: number;
  speed?: number;
  time?: number;
}

@Injectable({
  providedIn: 'root',
})
export class PositionService {

  public get positionState(): PositionState | undefined {
    return this.positionStateSource.value;
  }

  public get positionStateChange(): Observable<PositionState | undefined> {
    return this.positionStateSource.asObservable();
  }

  public get position(): Position | undefined {
    return this.positionChangeSource.value;
  }

  public get positionChange(): Observable<Position | undefined> {
    return this.positionChangeSource.asObservable();
  }

  private positionStateSource = new BehaviorSubject<PositionState | undefined>(undefined);
  private positionChangeSource = new BehaviorSubject<Position | undefined>(undefined);

  constructor(
    @Optional() @SkipSelf() readonly instance: PositionService
  ) {
    if (instance) {
      throw new SingletonError(this);
    }

    if (!navigator.geolocation) {
      this.positionStateSource.next('UNSUPPORTED');
      return;
    }

    navigator.geolocation.getCurrentPosition(this.onPositionSuccess, this.onPositionFailure);
    navigator.geolocation.watchPosition(this.onPositionSuccess,this.onPositionFailure);
  }

  private onPositionSuccess = (position: GeolocationPosition) => {
    this.positionStateSource.next('AVAILABLE');
    this.positionChangeSource.next({
      latitude: position.coords.latitude,
      longitude: position.coords.longitude,
      altitude: position.coords.altitude ? position.coords.altitude : undefined,
      heading: position.coords.heading ? position.coords.heading : undefined,
      speed: position.coords.speed ? position.coords.speed : undefined,
      time: position.timestamp,
    });
  };

  private onPositionFailure = (error: GeolocationPositionError) => {
    if (error.code == 1) {
      this.positionStateSource.next('BLOCKED');
    } else {
      this.positionStateSource.next('UNAVAILABLE');
    }
  };
}
