// Import Angular stuff

import { Injectable, Optional, SkipSelf } from '@angular/core';

// Import services used by the TokenService

import { LocalStorage } from './local-storage';
import { SingletonError } from '../errors';

const ACCESS_TOKEN = 'accessToken';
const REFRESH_TOKEN = 'refreshToken';

@Injectable({
  providedIn: 'root',
})
export class TokenService {
  constructor(private readonly storage: LocalStorage, @Optional() @SkipSelf() readonly instance: LocalStorage) {
    if (instance) {
      throw new  SingletonError(this);
    }
  }

  public getAccessToken(): string | undefined {
    return this.storage.get(ACCESS_TOKEN);
  }

  public setAccessToken(token: string | undefined): void {
    this.storage.set(ACCESS_TOKEN, token);
  }

  public getRefreshToken(): string | undefined {
    return this.storage.get(REFRESH_TOKEN);
  }

  public setRefreshToken(token: string | undefined): void {
    this.storage.set(REFRESH_TOKEN, token);
  }
}
