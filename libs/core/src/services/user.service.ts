// Import Angular stuff

import { Injectable, Optional, SkipSelf } from '@angular/core';

// Import RxJS stuff

import { Subject, Observable } from 'rxjs';

import { SingletonError } from '../errors';

// Import services used by the UserService

import { LocalStorage } from './local-storage';

const USER_KEY = 'currentUser';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private userChangeSource: Subject<any> = new Subject<any>();

  constructor(private readonly storage: LocalStorage, @Optional() @SkipSelf() readonly instance: UserService) {
    if (instance) {
      throw new SingletonError(this);
    }
  }
  
  public get userChange(): Observable<any> {
    return this.userChangeSource.asObservable();
  }

  public getCurrentUser(): any {
    return this.storage.get(USER_KEY);
  }

  public setCurrentUser(user: any): void {
    if (user !== this.getCurrentUser()) {
      this.userChangeSource.next(user);
    }

    this.storage.set(USER_KEY, user);
  }
}
