// Import Angular stuff

import { Injectable, Optional, SkipSelf } from '@angular/core';

import { SingletonError } from '../errors';

@Injectable({
  providedIn: 'root',
})
export class WindowService {
  constructor(@Optional() @SkipSelf() readonly instance: WindowService) {
    if (instance) {
      throw new  SingletonError(this);
    }
  }
  
  public getHostname(): string {
    return window.location.hostname;
  }
}
