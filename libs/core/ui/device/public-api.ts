/**
 * Public surface API of @public/core/ui/device
 */

export { Device, ScreenSize, Orientation } from './src/services';
