import { Injectable, Optional, SkipSelf } from '@angular/core';

import { BreakpointObserver, MediaMatcher } from '@angular/cdk/layout';

import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

import { SingletonError } from '@public/core';

const small = '(max-width: 600px)';
const large = '(min-width: 840px)';

export type ScreenSize = 'Small' | 'Medium' | 'Large';

export type Orientation = 'Landscape' | 'Portrait';

@Injectable({
  providedIn: 'root',
})
export class Device {
  public get hasTouch(): boolean {
    return this.mediaMatcher.matchMedia('(hover: none) and (pointer: coarse)').matches;
  }

  public get hasPointer(): boolean {
    return this.mediaMatcher.matchMedia('(hover: hover) and (pointer: fine)').matches;
  }

  public get hasStylus(): boolean {
    return this.mediaMatcher.matchMedia('(hover: none) and (pointer: fine)').matches;
  }

  public get screenSize(): ScreenSize {
    if (this.mediaMatcher.matchMedia(small).matches) {
      return 'Small';
    }
    if (this.mediaMatcher.matchMedia(large).matches) {
      return 'Large';
    }
    return 'Medium';
  }

  public get screenSizeChange(): Observable<ScreenSize> {
    return this.breakpointObserver
      .observe([small, large])
      .pipe(
        map(() =>
          this.breakpointObserver.isMatched(small)
            ? 'Small'
            : this.breakpointObserver.isMatched(large)
            ? 'Large'
            : 'Medium'
        )
      );
  }

  public get orientation(): Orientation {
    return this.mediaMatcher.matchMedia('(orientation: portrait)').matches ? 'Portrait' : 'Landscape';
  }

  public get orientationChange(): Observable<Orientation> {
    return this.breakpointObserver
      .observe(['(orientation: portrait)', '(orientation: landscape)'])
      .pipe(map(() => (this.breakpointObserver.isMatched('(orientation: portrait)') ? 'Portrait' : 'Landscape')));
  }

  constructor(
    private readonly breakpointObserver: BreakpointObserver,
    private readonly mediaMatcher: MediaMatcher,
    @Optional() @SkipSelf() readonly instance: Device
  ) {
    if (instance) {
      throw new SingletonError(this);
    }
  }
}
