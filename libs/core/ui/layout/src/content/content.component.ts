// Import Angular stuff

import { Component } from '@angular/core';

@Component({
  selector: 'app-content, app-main, main',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent {
}
