// Import Angular stuff

import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-footer, footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent { 
  @Input()
  public color: 'primary'| 'accent' | 'none' = 'none';

}
