// Import Angular stuff

import { Component, Input, HostBinding } from '@angular/core';

import {ThemePalette} from '@angular/material/core';

@Component({
  selector: 'app-header, header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  @Input()
  public color: 'primary'| 'accent' | 'none' = 'none';
}
