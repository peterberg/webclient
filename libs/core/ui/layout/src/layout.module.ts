// Import Angular stuff

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Import pipes, services, interceptors, components and directives

import { LayoutComponent } from './layout';
import { HeaderComponent } from './header';
import { FooterComponent } from './footer';
import { ContentComponent } from './content';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    LayoutComponent,
    HeaderComponent,
    FooterComponent,
    ContentComponent
  ],
  exports: [
    LayoutComponent,
    HeaderComponent,
    FooterComponent,
    ContentComponent
  ]
})
export class LayoutModule {}
