/**
 * Public surface API of @public/core/ui/sidenav
 */

 export { SidenavModule } from './src/sidenav.module';

export { Sidenav, SidenavMode, SidenavState, SIDENAV_CONFIG } from './src/sidenav';
export { SidenavComponent } from './src/sidenav.component';
export { SidenavContent } from './src/sidenav-content';
