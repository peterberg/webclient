import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[sidenav-content]',
})
export class SidenavContentDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
