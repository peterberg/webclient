// Import Angular stuff

import { Component, Type, ViewChild, OnInit, OnDestroy, HostListener, ComponentFactoryResolver } from '@angular/core';

// Import RxJS stuff

import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { SidenavContentDirective } from './sidenav-content.directive';
import { SidenavContent } from './sidenav-content';
import { Sidenav } from './sidenav';

@Component({
  selector: 'app-sidenav',
  templateUrl: 'sidenav.component.html',
  styleUrls: ['sidenav.component.scss'],
})
export class SidenavComponent implements OnInit, OnDestroy {
  @ViewChild(SidenavContentDirective, { static: true })
  public content!: SidenavContentDirective;

  private destroyer: ReplaySubject<boolean> = new ReplaySubject(1);

  constructor(private sidenav: Sidenav) {}

  /**
   *
   */

  public ngOnInit(): void {
    this.sidenav.contentChange.pipe(takeUntil(this.destroyer)).subscribe((component: Type<SidenavContent> | undefined) => {
      this.loadContent(component);
    });
  }

  /**
   *
   */

  public ngOnDestroy(): void {
    this.destroyer.next(true);
    this.destroyer.complete();
  }

  /**
   *
   * @param component
   */

  public loadContent(component: Type<SidenavContent> | undefined) {
    if (this.content) {
      const viewContainerRef = this.content.viewContainerRef;
      viewContainerRef.clear();

      if (component) {
        viewContainerRef.createComponent(component);
      }
    }
  }
}
