// Import Angular, CDK and Marerial stuff

import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MatSidenavModule } from '@angular/material/sidenav';

// Import pipes, services, interceptors, components and directives

import { SidenavComponent } from './sidenav.component';
import { SidenavContentDirective } from './sidenav-content.directive';

@NgModule({
  imports: [
    CommonModule,
    RouterModule, 

    MatSidenavModule
  ],
  declarations: [
    SidenavComponent,
    SidenavContentDirective
  ],
  exports: [
    SidenavComponent
  ]
})
export class SidenavModule {}
