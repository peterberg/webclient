// Import Angular stuff

import { Injectable, Type, OnDestroy, Inject, InjectionToken, Optional } from '@angular/core';
import { Router, Event, NavigationEnd, NavigationError } from '@angular/router';

import { BreakpointObserver, BreakpointState, Breakpoints } from '@angular/cdk/layout';

// Import RxJS stuff

import { BehaviorSubject, ReplaySubject, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { SidenavContent } from './sidenav-content';

export type SidenavState = 'open' | 'closed';
export type SidenavMode = 'over' | 'side' | 'push';

/** Injection token that can be used to configure the sidenav's behavior. */

export const SIDENAV_CONFIG = new InjectionToken<any>('SidenavConfig');

const defaultSidenavOver = [Breakpoints.Handset, Breakpoints.TabletPortrait];

@Injectable({
  providedIn: 'root',
})
export class Sidenav implements OnDestroy {
  public breakpointState!: BreakpointState;

  private contentChangeSource = new BehaviorSubject<Type<SidenavContent> | undefined>(undefined);

  private stateChangeSource = new BehaviorSubject<SidenavState | undefined>(undefined);
  private modeChangeSource = new BehaviorSubject<SidenavMode | undefined>(undefined);

  private destroyer: ReplaySubject<boolean> = new ReplaySubject(1);

  public get content(): Type<SidenavContent> | undefined {
    return this.contentChangeSource.value;
  }

  public set content(component: Type<SidenavContent> | undefined) {
    this.contentChangeSource.next(component);
  }

  public get state(): SidenavState | undefined {
    return this.stateChangeSource.value;
  }

  public set state(state: SidenavState | undefined) {
    this.stateChangeSource.next(state);
  }

  public get mode(): SidenavMode | undefined {
    return this.modeChangeSource.value;
  }

  public set mode(mode: SidenavMode | undefined) {
    this.modeChangeSource.next(mode);
  }

  public get contentChange(): Observable<Type<SidenavContent> | undefined> {
    return this.contentChangeSource.asObservable();
  }

  public get stateChange(): Observable<SidenavState | undefined> {
    return this.stateChangeSource.asObservable();
  }

  public get modeChange(): Observable<SidenavMode | undefined> {
    return this.modeChangeSource.asObservable();
  }

  constructor(
    breakpointObserver: BreakpointObserver,
    router: Router,
    @Optional() @Inject(SIDENAV_CONFIG) sidenavOver: string | string[]
  ) {
    if(!sidenavOver) {
      sidenavOver = defaultSidenavOver;
    }

    if(sidenavOver.length>0) {
      if (breakpointObserver.isMatched(sidenavOver)) {
        this.mode = 'over';
        this.close();
      } else {
        this.mode = 'side';
        this.open();
      }

      breakpointObserver
        .observe(sidenavOver)
        .pipe(takeUntil(this.destroyer))
        .subscribe((state: BreakpointState) => {
          this.breakpointState = state;

          if (state.matches) {
            this.mode = 'over';
            this.close();
          } else {
            this.mode = 'side';
            this.open();
          }
        });
    }

    router.events.pipe(takeUntil(this.destroyer)).subscribe((event: Event) => {
      if (this.mode === 'over' && (event instanceof NavigationEnd || event instanceof NavigationError)) {
        this.close();
      }
    });
  }

  public ngOnDestroy(): void {
    this.destroyer.next(true);
    this.destroyer.complete();
  }
  
  public open() {
    this.state = 'open';
  }

  public close() {
    this.state = 'closed';
  }

  public toggle() {
    this.state = this.state === 'open' ? 'closed' : 'open';
  }
}
