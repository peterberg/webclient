/**
 * Public surface API of @public/core/ui/theme
 */

export { ThemeService } from './src/services';
