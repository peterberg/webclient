import { Injectable, Inject, InjectionToken, Optional, SkipSelf } from '@angular/core';

import { Observable, BehaviorSubject } from 'rxjs';

import { LocalStorage } from '@public/core';

export type Theme = 'default' | 'dark';

export const DEFAULT_THEME = new InjectionToken<Theme>('default_theme');

const THEME_KEY = 'theme';

@Injectable({
  providedIn: 'root',
})
export class ThemeService {
  private themeChangeSource = new BehaviorSubject<string |undefined>(undefined);

  public get theme(): string | undefined{
    return this.themeChangeSource.value;
  }

  public set theme(value: string | undefined) {
    const htmlRoot = document.getElementsByTagName('html')[0];

    if (htmlRoot) {
      // Remove previously set theme class.

      if (!!this.storage.get(THEME_KEY) && htmlRoot.classList.contains(`${this.storage.get(THEME_KEY)}-theme`)) {
        htmlRoot.classList.remove(`${this.storage.get(THEME_KEY)}-theme`);
      }

      // Set theme class on document's HTML element unless the theme is "default" or empty .

      if (!!value && value !== 'default') {
        htmlRoot.classList.add(`${value}-theme`);
      }
    }

    // Store applied theme in local storage for later use and emit change event.

    this.storage.set(THEME_KEY, value);
    this.themeChangeSource.next(value);
  }

  public get themeChange(): Observable<string | undefined> {
    return this.themeChangeSource.asObservable();
  }

  constructor(
    private readonly storage: LocalStorage,
    @Optional() @Inject(DEFAULT_THEME) defaultTheme: Theme,
    @Optional() @SkipSelf() readonly instance: ThemeService
  ) {
    if (instance) {
      throw new Error(`${this.constructor.name} already instantiated. Provide it only in the root module.`);
    }

    if (!this.storage.get(THEME_KEY)) {
      this.storage.set(THEME_KEY, defaultTheme ? defaultTheme : 'default');
    }

    this.theme = this.storage.get(THEME_KEY);
  }
}
