import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[toolbar-content]',
})
export class ToolbarContentDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
