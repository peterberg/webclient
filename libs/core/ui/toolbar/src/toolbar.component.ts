import { Component, Type, ViewChild, OnInit, OnDestroy, ComponentFactoryResolver } from '@angular/core';
import { Title } from '@angular/platform-browser';

// Import RxJS stuff

import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { ToolbarContentDirective } from './toolbar-content.directive';
import { ToolbarContent } from './toolbar-content';
import { Toolbar } from './toolbar';

import { Sidenav } from '@public/core/ui/sidenav';

@Component({
  selector: 'app-toolbar',
  templateUrl: 'toolbar.component.html',
  styleUrls: ['toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit, OnDestroy {
  @ViewChild(ToolbarContentDirective, { static: true })
  content!: ToolbarContentDirective;

  private destroyer: ReplaySubject<boolean> = new ReplaySubject(1);

  constructor(
    public titleService: Title,
    public toolbar: Toolbar,
    public sidenav: Sidenav
  ) {}

  /**
   *
   */

  public ngOnInit() {
    this.toolbar.contentChange.pipe(takeUntil(this.destroyer)).subscribe((component: Type<ToolbarContent> | undefined) => {
      this.loadContent(component);
    });
  }

  /**
   *
   */

  public ngOnDestroy() {
    this.destroyer.next(true);
    this.destroyer.complete();
  }

  public loadContent(component: Type<ToolbarContent> | undefined) {
    if (!!this.content) {
      const viewContainerRef = this.content.viewContainerRef;
      viewContainerRef.clear();

      if (!!component) {
        viewContainerRef.createComponent(component);
      }
    }
  }
}
