// Import Angular, CDK and Marerial stuff

import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';

import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';

// Import pipes, services, interceptors, components and directives

import { ToolbarComponent } from './toolbar.component';
import { ToolbarContentDirective } from './toolbar-content.directive';

@NgModule({
  imports: [
    CommonModule,

    MatIconModule,
    MatToolbarModule
  ],
  declarations: [
    ToolbarComponent,
    ToolbarContentDirective
  ],
  exports: [
    ToolbarComponent
  ]
})
export class ToolbarModule {}
