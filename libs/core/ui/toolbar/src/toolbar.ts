import { Injectable, Type, Optional, SkipSelf } from '@angular/core';

import { BehaviorSubject, Observable } from 'rxjs';

import { ToolbarContent } from './toolbar-content';

@Injectable({
  providedIn: 'root'
})
export class Toolbar {
  private contentChangeSource = new BehaviorSubject<Type<ToolbarContent> | undefined>(undefined);

  public get content(): Type<ToolbarContent> | undefined {
    return this.contentChangeSource.value;
  }

  public set content(component: Type<ToolbarContent> | undefined) {
    this.contentChangeSource.next(component);
  }

  public get contentChange(): Observable<Type<ToolbarContent> | undefined> {
    return this.contentChangeSource.asObservable();
  }
  
  constructor( @Optional() @SkipSelf() readonly instance: Toolbar) {
    if (instance) {
      throw new Error(`${this.constructor.name} already instantiated. Provide it only in the root module.`);
    }
  }
}
